A program for filtering YT queries i.e. selecting the required number of queries that give the most unique results.

To restore the environment:
    pip install -r requirements.txt

To run the program:
    python ./qreduce.py --api-key-file PATH_TO_API_KEY_FILE --queries-file INPUT_FILE --output-file OUTPUT_FILE --num-queries NUM_QUERIES

For help message:
    python ./qreduce.py -h

Files:
    qreduce.py: main program
    yt.py: implementation of requests to Youtube API
    ytquery.py: implementation of filtering algorithm
    api_key.txt: default file with API KEY
    pickledqueries.py: auxiliary program for creating pickled results of queries (to avoid quota limitation)

