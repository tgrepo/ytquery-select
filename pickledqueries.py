import pickle
import argparse
from yt import Youtube
from googleapiclient.errors import HttpError


parser = argparse.ArgumentParser()
parser.add_argument('-k', '--api-key-file', default='api_key_list.txt', help='path to the file with DEVELOPER API KEYs')
parser.add_argument('-q', '--queries-file', default='text_queries_input.txt', help='path to the queries file')
parser.add_argument('-m', '--max-results', default=25, help='max results for a query [25:50]')
args = parser.parse_args()


try:
    with open(args.queries_file, encoding='utf8') as f:
        qlines = [line.strip() for line in f.readlines() if line.strip()]
except EnvironmentError:
    raise Exception('invalid queries file "{}"'.format(args.queries_file))

try:
    with open(args.api_key_file, encoding='utf8') as f:
        klines = [line.strip() for line in f.readlines() if line.strip()]
except EnvironmentError:
    raise Exception('invalid API KEYs file "{}"'.format(args.api_key_file))

keys_iter = iter(klines)
youtube = Youtube(next(keys_iter), args.max_results)
queries = dict()
try:
    for query in qlines:
        try:
            video_ids = youtube.search(query)
            if video_ids:
                queries[query] = video_ids
        except HttpError:
            youtube = Youtube(next(keys_iter), args.max_results)
finally:
    pickle.dump(queries, open('queries1.p', "wb"))

