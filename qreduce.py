import argparse
import traceback
import sys
from yt import Youtube
from ytquery import Queries, VideoIdUnion, QueriesSelector, Printer
from googleapiclient.errors import HttpError


def exception_to_string(excp):
    stack = traceback.extract_stack()[:-3] + traceback.extract_tb(excp.__traceback__)
    pretty = traceback.format_list(stack)
    return ''.join(pretty) + '\n  {} {}'.format(excp.__class__,excp)


def get_args():
    """
    Create command line arguments parser. Parse command line arguments.
    :return: dictionary-like parsed command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--api-key-file', default='api_key.txt', help='path to the file with DEVELOPER API KEY')
    parser.add_argument('-q', '--queries-file', default='text_queries_input.txt', help='path to the queries file')
    parser.add_argument('-m', '--max-results', default=25, help='max results for a query [25:50]')

    # Please, PAY ATTENTION to the assumption
    parser.add_argument('-n', '--num-queries', default=50, help='reduce to NUM-QUERIES best queries'
                                                                'ASSUMPTION:'
                                                                ' requested reduced list is less than original')
    parser.add_argument('-o', '--output-file', required=False, help='output file for best queries')
    parser.add_argument('-p', '--use-pickled-queries', action='store_true', help='use pickled queries instead of using API')
    return parser.parse_args()


def get_key(fname):
    """
    Read API KEY form a file.
    :param str fname: file path where API KEY stored.
    :return str:  API KEY
    """
    try:
        with open(fname) as f:
            lines = f.readlines()
    except EnvironmentError:
        raise Exception('invalid API KEY file "{}"'.format(fname))

    for line in lines:
        line = line.strip()
        if len(line):
            return line
    raise Exception("API KEY file is empty")


def main():
    args = get_args()
    api_key = get_key(args.api_key_file)
    youtube = Youtube(api_key, args.max_results)
    queries = Queries(args.queries_file, youtube, use_pickled=args.use_pickled_queries)
    vid_union = VideoIdUnion(queries)
    query_selector = QueriesSelector(queries, vid_union)
    with Printer(args.output_file) as printer:
        for best_query in query_selector.most_informative(args.num_queries):
            printer.print(best_query)


if __name__ == '__main__':
    try:
        main()
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content), file=sys.stderr)
        print(exception_to_string(e), file=sys.stderr)
        exit(1)
    except Exception as e:
        print(exception_to_string(e), file=sys.stderr)
        print('ABNORMAL TERMINATION, see the stack trace above', file=sys.stderr)
        exit(2)
