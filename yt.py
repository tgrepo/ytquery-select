#!/usr/bin/python
'''
source: https://developers.google.com/youtube/v3/quickstart/python
'''
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


class Youtube:
    API_SERVICE_NAME = 'youtube'
    API_VERSION = 'v3'

    def __init__(self, developer_key, max_results):
        self.max_results = max_results
        self._developer_key = developer_key

    def connect(self):
        self._youtube = build(Youtube.API_SERVICE_NAME,
                              Youtube.API_VERSION,
                              developerKey=self._developer_key)

    def search(self, query):

        search_response = self._youtube.search().list(
            q=query,
            part='id',
            maxResults=self.max_results
        ).execute()

        videos = []
        channels = []
        playlists = []

        for search_result in search_response.get('items', []):
            if search_result['id']['kind'] == 'youtube#video':
                videos.append(search_result['id']['videoId'])
            elif search_result['id']['kind'] == 'youtube#channel':
                channels.append(search_result['id']['channelId'])
            elif search_result['id']['kind'] == 'youtube#playlist':
                playlists.append(search_result['id']['playlistId'])
        # I am going to deal only with video ids
        return videos


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--api-key-file', default='api_key.txt', help='path to the file with DEVELOPER API KEY')
    parser.add_argument('-m', '--max-results', default=25, help='max results for a query [25:50]')
    args = parser.parse_args()
    with open(args.api_key_file) as f:
        api_key = f.readline().strip()
    try:
        yt = Youtube(api_key, args.max_results)
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
        exit(1)
    print(yt.search('test'))
