import sys
import pickle


class Queries:
    """
     Queries class builds a dictionary of type "query": "list of video ids".
     The data may be loaded using Developer API (Youtube class) or from pre-stored pickle file (to avoid quota limitation).
     This class provides convenience functionality of an iterable.
    """

    def __init__(self, fname, youtube, use_pickled=False):
        self._queries = None
        self._fname = fname
        self._youtube = youtube
        self._use_pickled = use_pickled

    def _build_queries(self):
        if self._use_pickled:
            self. _build_queries_from_pickle()
        else:
            self._youtube.connect()
            self._build_queries_from_network()

    def _build_queries_from_pickle(self):
        with open('queries.p', 'rb') as f:
            self._queries = pickle.load(f)

    def _build_queries_from_network(self):
        try:
            #with open(self._fname, errors='ignore') as f:
            with open(self._fname, encoding='utf8') as f:
                lines = f.readlines()
        except EnvironmentError:
            raise Exception('invalid queries file "{}"'.format(self._fname))

        self._queries = dict()
        for line in lines:
            query = line.strip()
            # we do not want empty queries
            if query:
                video_ids = self._youtube.search(query)
                # we do not want empty results
                if video_ids:
                    self._queries[query] = video_ids

    def __iter__(self):
        if self._queries is None:
            self._build_queries()
        for k, v in self._queries.items():
            yield k, v

    def remove(self, query):
        del self._queries[query]

    def __len__(self):
        return len(self._queries)


class VideoIdUnion:
    """
    This class holds a dictionary of type: "video id": "set of queries, results of witch contain this video id"
    """

    def __init__(self, queries):
        self._queries = queries
        self._union = {}
        for query, video_ids in self._queries:
            self.add(query, video_ids)

    def add(self, query, video_ids):
        for video_id in video_ids:
            try:
                self._union[video_id].add(query)
            except:
                self._union[video_id] = {query}

    def subtract(self, query, video_ids):
        for video_id in video_ids:
            assert query in self._union[video_id]
            self._union[video_id].remove(query)
            if len(self._union[video_id]) == 0:
                del self._union[video_id]

    def keys(self):
        return self._union.keys()

    def __len__(self):
        return len(self._union)


class QueriesSelector:
    """
    This class implements the queries selection algorithm.
    """

    def __init__(self, queries, vid_union):
        """
        :param Queries queries:
        :param VideoIdUnion vid_union:
        """
        self._queries = queries
        self._vid_union = vid_union
        self._max_score = len(vid_union)

    def _get_score(self, query, video_ids):
        """
        Calculate the score of a query.
        Algorithm:
            1) subtract the contribution of the query to Video Ids Union
            2) calculate the number of video ids uniquely contributed by this query
            3) add the query again to Video Ids Union
            4) return the calculated number
        :param str query:
        :param list video_ids: list of video ids corresponding to this query
        :return int: score
        """
        self._vid_union.subtract(query, video_ids)
        union = set(self._vid_union.keys())
        score = len(set(video_ids) - union)
        self._vid_union.add(query, video_ids)
        return score

    def _do_selection(self, num):
        """
        This is the main algorithm where the best "num" queries selected.
        Algorithm:
        while number of queries greater than NUM
            for each query
                calculate score
            remove worst query
            update Video Ids Union

        :param int num: number of queries to reduce to
        """

        class QueryContainer:
            """
            Convenience class for storing worst query
            """
            def __init__(self, query, video_ids):
                self.query = query
                self.video_ids = video_ids

        while len(self._queries) > num:
            min_score = self._max_score
            for query, vids in self._queries:
                score = self._get_score(query, vids)
                if score <= min_score:
                    min_score = score
                    worst_query = QueryContainer(query, vids)

            self._queries.remove(worst_query.query)
            self._vid_union.subtract(worst_query.query, worst_query.video_ids)

    def most_informative(self, num):
        self._do_selection(num)
        for query, _ in self._queries:
            yield query


class Printer:
    """
    Convenience class for printing
    """
    def __init__(self, fname=None):
        self._fname = fname

    def __enter__(self):
        if self._fname is None:
            self._out = sys.stdout
        else:
            self._out = open(self._fname, 'w', encoding='utf8')
        return self

    def print(self, s):
        print(s, file=self._out)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._fname is not None:
            self._out.close()

